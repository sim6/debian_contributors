# coding: utf8
# Debian Contributors unit tests
#
# Copyright (C) 2014  Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# Create your views here.

from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals
from django.test import TestCase
#from django.test.utils import override_settings
from sources.test_common import *
import datetime

class APCTestCase(SimpleSourceFixtureMixin, TestCase):
    def setUp(self):
        super(APCTestCase, self).setUp()
        self.ctype1 = cmodels.ContributionType(source=self.source, name="faffer", desc="faffer_desc", contrib_desc="faffer_cdesc")
        self.ctype1.save()

        for u in "admin", "dd", "dd1", "alioth", "alioth1":
            i1 = getattr(self, "id_{}_user".format(u))
            i2 = getattr(self, "id_{}_home".format(u))

            # Make a contribution to another data source for each identifier
            cmodels.Contribution.objects.create(type=self.ctype1, identifier=i1,
                begin=datetime.date(2014, 1, 1),
                until=datetime.date(2014, 1, 15))
            cmodels.Contribution.objects.create(type=self.ctype1, identifier=i2,
                begin=datetime.date(2014, 3, 1),
                until=datetime.date(2014, 3, 15))

    def test_aggregate_all(self):
        self.assertFalse(cmodels.AggregatedPersonContribution.objects.exists())

        # Recompute all
        cmodels.AggregatedPersonContribution.recompute()

        self.assertEquals(cmodels.AggregatedPersonContribution.objects.count(), 10)

        c = list(cmodels.AggregatedPersonContribution.objects.filter(user=self.user_admin).order_by("ctype__name"))
        self.assertEquals(len(c), 2)

        self.assertEquals(c[0].user, self.user_admin)
        self.assertEquals(c[0].ctype, self.ctype1)
        self.assertEquals(c[0].begin, datetime.date(2014, 1, 1))
        self.assertEquals(c[0].until, datetime.date(2014, 3, 15))

        self.assertEquals(c[1].user, self.user_admin)
        self.assertEquals(c[1].ctype, self.ctype)
        self.assertEquals(c[1].begin, datetime.date(2014, 2, 1))
        self.assertEquals(c[1].until, datetime.date(2014, 2, 15))

    def test_aggregate_user(self):
        self.assertFalse(cmodels.AggregatedPersonContribution.objects.exists())

        # Recompute all
        cmodels.AggregatedPersonContribution.recompute(user=self.user_admin)

        self.assertEquals(cmodels.AggregatedPersonContribution.objects.count(), 2)

        c = list(cmodels.AggregatedPersonContribution.objects.filter(user=self.user_admin).order_by("ctype__name"))
        self.assertEquals(len(c), 2)

        self.assertEquals(c[0].user, self.user_admin)
        self.assertEquals(c[0].ctype, self.ctype1)
        self.assertEquals(c[0].begin, datetime.date(2014, 1, 1))
        self.assertEquals(c[0].until, datetime.date(2014, 3, 15))

        self.assertEquals(c[1].user, self.user_admin)
        self.assertEquals(c[1].ctype, self.ctype)
        self.assertEquals(c[1].begin, datetime.date(2014, 2, 1))
        self.assertEquals(c[1].until, datetime.date(2014, 2, 15))

    def test_aggregate_ctype(self):
        self.assertFalse(cmodels.AggregatedPersonContribution.objects.exists())

        # Recompute all
        cmodels.AggregatedPersonContribution.recompute(ctype=self.ctype1)

        self.assertEquals(cmodels.AggregatedPersonContribution.objects.count(), 5)

        c = list(cmodels.AggregatedPersonContribution.objects.filter(user=self.user_admin).order_by("ctype__name"))
        self.assertEquals(len(c), 1)

        self.assertEquals(c[0].user, self.user_admin)
        self.assertEquals(c[0].ctype, self.ctype1)
        self.assertEquals(c[0].begin, datetime.date(2014, 1, 1))
        self.assertEquals(c[0].until, datetime.date(2014, 3, 15))

    def test_aggregate_user_ctype(self):
        self.assertFalse(cmodels.AggregatedPersonContribution.objects.exists())

        # Recompute all
        cmodels.AggregatedPersonContribution.recompute(user=self.user_admin, ctype=self.ctype1)

        self.assertEquals(cmodels.AggregatedPersonContribution.objects.count(), 1)

        c = list(cmodels.AggregatedPersonContribution.objects.filter(user=self.user_admin).order_by("ctype__name"))
        self.assertEquals(len(c), 1)

        self.assertEquals(c[0].user, self.user_admin)
        self.assertEquals(c[0].ctype, self.ctype1)
        self.assertEquals(c[0].begin, datetime.date(2014, 1, 1))
        self.assertEquals(c[0].until, datetime.date(2014, 3, 15))

    def test_aggregate_partial(self):
        self.assertFalse(cmodels.AggregatedPersonContribution.objects.exists())

        # Compute all
        cmodels.AggregatedPersonContribution.recompute()

        # Tweak a contribution
        c = cmodels.Contribution.objects.get(type=self.ctype1, identifier=self.id_admin_home)
        c.begin=datetime.date(2014, 3, 17)
        c.until=datetime.date(2014, 3, 19)
        c.save()

        # Reaggregate only a user/ctype
        cmodels.AggregatedPersonContribution.recompute(user=self.user_admin, ctype=self.ctype1)

        # Still there are all the other entries
        self.assertEquals(cmodels.AggregatedPersonContribution.objects.count(), 10)

        c = list(cmodels.AggregatedPersonContribution.objects.filter(user=self.user_admin).order_by("ctype__name"))
        self.assertEquals(len(c), 2)

        self.assertEquals(c[0].user, self.user_admin)
        self.assertEquals(c[0].ctype, self.ctype1)
        self.assertEquals(c[0].begin, datetime.date(2014, 1, 1))
        self.assertEquals(c[0].until, datetime.date(2014, 3, 19))

        self.assertEquals(c[1].user, self.user_admin)
        self.assertEquals(c[1].ctype, self.ctype)
        self.assertEquals(c[1].begin, datetime.date(2014, 2, 1))
        self.assertEquals(c[1].until, datetime.date(2014, 2, 15))

    def test_hide_user(self):
        self.assertFalse(cmodels.AggregatedPersonContribution.objects.exists())

        self.user_dd.hidden = True
        self.user_dd.save()

        # Recompute all
        cmodels.AggregatedPersonContribution.recompute()
        self.assertEquals(cmodels.AggregatedPersonContribution.objects.count(), 8)

        # There are no entries for identifiers of that user
        self.assertFalse(cmodels.AggregatedPersonContribution.objects.filter(user=self.user_dd).exists())

        # The rest is still ok
        c = list(cmodels.AggregatedPersonContribution.objects.filter(user=self.user_admin).order_by("ctype__name"))
        self.assertEquals(len(c), 2)

        self.assertEquals(c[0].user, self.user_admin)
        self.assertEquals(c[0].ctype, self.ctype1)
        self.assertEquals(c[0].begin, datetime.date(2014, 1, 1))
        self.assertEquals(c[0].until, datetime.date(2014, 3, 15))

        self.assertEquals(c[1].user, self.user_admin)
        self.assertEquals(c[1].ctype, self.ctype)
        self.assertEquals(c[1].begin, datetime.date(2014, 2, 1))
        self.assertEquals(c[1].until, datetime.date(2014, 2, 15))

    def test_hide_ident(self):
        self.assertFalse(cmodels.AggregatedPersonContribution.objects.exists())

        self.id_admin_home.hidden = True
        self.id_admin_home.save()

        # Recompute all
        cmodels.AggregatedPersonContribution.recompute()
        self.assertEquals(cmodels.AggregatedPersonContribution.objects.count(), 10)

        # There are entries for that user, limited to the visible identifiers
        self.assertTrue(cmodels.AggregatedPersonContribution.objects.filter(user=self.user_admin).exists())

        # The rest is still ok
        c = list(cmodels.AggregatedPersonContribution.objects.filter(user=self.user_admin).order_by("ctype__name"))
        self.assertEquals(len(c), 2)

        self.assertEquals(c[0].user, self.user_admin)
        self.assertEquals(c[0].ctype, self.ctype1)
        self.assertEquals(c[0].begin, datetime.date(2014, 1, 1))
        self.assertEquals(c[0].until, datetime.date(2014, 1, 15))

        self.assertEquals(c[1].user, self.user_admin)
        self.assertEquals(c[1].ctype, self.ctype)
        self.assertEquals(c[1].begin, datetime.date(2014, 2, 1))
        self.assertEquals(c[1].until, datetime.date(2014, 2, 15))

    def test_hide_source(self):
        self.assertFalse(cmodels.AggregatedPersonContribution.objects.exists())

        s = self.source.get_settings(self.user_dd, create=True)
        s.hidden = True
        s.save()

        # Recompute all
        cmodels.AggregatedPersonContribution.recompute()
        self.assertEquals(cmodels.AggregatedPersonContribution.objects.count(), 8)

        # There are entries for that user, limited to the visible identifiers
        self.assertFalse(cmodels.AggregatedPersonContribution.objects.filter(user=self.user_dd).exists())

        # The rest is still ok
        c = list(cmodels.AggregatedPersonContribution.objects.filter(user=self.user_admin).order_by("ctype__name"))
        self.assertEquals(len(c), 2)

        self.assertEquals(c[0].user, self.user_admin)
        self.assertEquals(c[0].ctype, self.ctype1)
        self.assertEquals(c[0].begin, datetime.date(2014, 1, 1))
        self.assertEquals(c[0].until, datetime.date(2014, 3, 15))

        self.assertEquals(c[1].user, self.user_admin)
        self.assertEquals(c[1].ctype, self.ctype)
        self.assertEquals(c[1].begin, datetime.date(2014, 2, 1))
        self.assertEquals(c[1].until, datetime.date(2014, 2, 15))


class APTestCase(SimpleSourceFixtureMixin, TestCase):
    def setUp(self):
        super(APTestCase, self).setUp()
        self.ctype1 = cmodels.ContributionType.objects.create(source=self.source, name="faffer", desc="faffer_desc", contrib_desc="faffer_cdesc")
        self.source2 = cmodels.Source.objects.create(name="test2", desc="other test source", url="http://www.example.org", auth_token="a")
        self.ctype2 = cmodels.ContributionType.objects.create(source=self.source2, name="faffer2", desc="faffer_desc2", contrib_desc="faffer_cdesc2")
        self.ctype21 = cmodels.ContributionType.objects.create(source=self.source2, name="faffer21", desc="faffer_desc21", contrib_desc="faffer_cdesc21")

    #@override_settings(DEBUG=True)
    def test_simple(self):
        cmodels.AggregatedPersonContribution.objects.create(
            user=self.user_admin,
            ctype=self.ctype,
            begin=datetime.date(2014, 1, 1),
            until=datetime.date(2014, 2, 1),
        )

        self.assertEquals(cmodels.AggregatedPerson.objects.count(), 0)

        cmodels.AggregatedPerson.recompute()

        self.assertEquals(cmodels.AggregatedPerson.objects.count(), 1)
        ap = cmodels.AggregatedPerson.objects.all()[0]
        self.assertEquals(ap.user, self.user_admin)
        self.assertEquals(ap.begin, datetime.date(2014, 1, 1))
        self.assertEquals(ap.until, datetime.date(2014, 2, 1))

    def test_two_sources(self):
        cmodels.AggregatedPersonContribution.objects.create(
            user=self.user_admin,
            ctype=self.ctype,
            begin=datetime.date(2014, 1, 1),
            until=datetime.date(2014, 2, 1),
        )
        cmodels.AggregatedPersonContribution.objects.create(
            user=self.user_admin,
            ctype=self.ctype2,
            begin=datetime.date(2014, 3, 1),
            until=datetime.date(2014, 4, 1),
        )

        self.assertEquals(cmodels.AggregatedPerson.objects.count(), 0)

        cmodels.AggregatedPerson.recompute()

        self.assertEquals(cmodels.AggregatedPerson.objects.count(), 1)
        ap = cmodels.AggregatedPerson.objects.all()[0]
        self.assertEquals(ap.user, self.user_admin)
        self.assertEquals(ap.begin, datetime.date(2014, 1, 1))
        self.assertEquals(ap.until, datetime.date(2014, 4, 1))

    def test_recompute(self):
        cmodels.AggregatedPersonContribution.objects.create(
            user=self.user_admin,
            ctype=self.ctype,
            begin=datetime.date(2014, 1, 1),
            until=datetime.date(2014, 2, 1),
        )
        cmodels.AggregatedPersonContribution.objects.create(
            user=self.user_dd,
            ctype=self.ctype,
            begin=datetime.date(2014, 1, 1),
            until=datetime.date(2014, 2, 1),
        )

        self.assertEquals(cmodels.AggregatedPerson.objects.count(), 0)

        # Recompute all
        cmodels.AggregatedPerson.recompute()

        self.assertEquals(cmodels.AggregatedPerson.objects.count(), 2)
        aps = list(cmodels.AggregatedPerson.objects.order_by("user__email"))
        self.assertEquals(aps[0].user, self.user_admin)
        self.assertEquals(aps[0].begin, datetime.date(2014, 1, 1))
        self.assertEquals(aps[0].until, datetime.date(2014, 2, 1))
        self.assertEquals(aps[1].user, self.user_dd)
        self.assertEquals(aps[1].begin, datetime.date(2014, 1, 1))
        self.assertEquals(aps[1].until, datetime.date(2014, 2, 1))

        # Add a new entry
        cmodels.AggregatedPersonContribution.objects.create(
            user=self.user_admin,
            ctype=self.ctype2,
            begin=datetime.date(2014, 3, 1),
            until=datetime.date(2014, 4, 1),
        )

        # Partial recompute
        cmodels.AggregatedPerson.recompute(user=self.user_admin)

        self.assertEquals(cmodels.AggregatedPerson.objects.count(), 2)
        aps = list(cmodels.AggregatedPerson.objects.order_by("user__email"))
        self.assertEquals(aps[0].user, self.user_admin)
        self.assertEquals(aps[0].begin, datetime.date(2014, 1, 1))
        self.assertEquals(aps[0].until, datetime.date(2014, 4, 1))
        self.assertEquals(aps[1].user, self.user_dd)
        self.assertEquals(aps[1].begin, datetime.date(2014, 1, 1))
        self.assertEquals(aps[1].until, datetime.date(2014, 2, 1))

        # Add a new entry
        cmodels.AggregatedPersonContribution.objects.create(
            user=self.user_dd,
            ctype=self.ctype21,
            begin=datetime.date(2014, 3, 1),
            until=datetime.date(2014, 3, 15),
        )

        # Total recompute
        cmodels.AggregatedPerson.recompute()

        self.assertEquals(cmodels.AggregatedPerson.objects.count(), 2)
        aps = list(cmodels.AggregatedPerson.objects.order_by("user__email"))
        self.assertEquals(aps[0].user, self.user_admin)
        self.assertEquals(aps[0].begin, datetime.date(2014, 1, 1))
        self.assertEquals(aps[0].until, datetime.date(2014, 4, 1))
        self.assertEquals(aps[1].user, self.user_dd)
        self.assertEquals(aps[1].begin, datetime.date(2014, 1, 1))
        self.assertEquals(aps[1].until, datetime.date(2014, 3, 15))


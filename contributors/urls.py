# coding: utf8
# Debian Contributors import/export
#
# Copyright (C) 2013--2014  Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals
from django.conf.urls import patterns, url
from importer import views as iviews
from . import views

urlpatterns = patterns('contributors.views',
    url(r'^year/(?P<year>\d+)$', "contributors", name="contributors_year"),
    url(r'^post$', iviews.post_source, name="contributors_post"),
    url(r'^flat$', 'contributors_flat', name="contributors_flat"),
    url(r'^new$', views.ContributorsNew.as_view(), name="contributors_new"),
    url(r'^sources/flat$', 'sources_flat', name="contributors_sources_flat"),
    url(r'^export/sources$', iviews.export_sources, name="contributors_export_sources"),
    url(r'^site_status$', 'site_status', name="site_status"),
    url(r'^claim/$', views.Claim.as_view(), name="contributors_claim"),
    url(r'^claim/identifiers/(?P<type>[a-z]+)/$', views.ClaimIdents.as_view(), name="contributors_claim_idents"),
    url(r'^claim/people/$', views.ClaimPeople.as_view(), name="contributors_claim_people"),
    # url(r'^gNHX0g5S$', 'zobel_dump_vars'),
)

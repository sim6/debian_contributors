# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('contributors', '0004_auto_20150208_1141'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contributiontype',
            name='name',
            field=models.CharField(help_text='Contribution type name, shown on the website and used as an identifier when submitting contribution data.', max_length=32, validators=[django.core.validators.RegexValidator('^[A-Za-z0-9][A-Za-z0-9._ -]*$', 'Name should start with a letter or number, and can contain letters, numbers, dots, spaces, underscores and minus signs')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='source',
            name='name',
            field=models.CharField(help_text='Data source name, shown on the website and used as a data source identifier when submitting contribution data.', unique=True, max_length=32, validators=[django.core.validators.RegexValidator('^[A-Za-z0-9][A-Za-z0-9._ -]*$', 'Name should start with a letter or number, and can contain letters, numbers, dots, spaces, underscores and minus signs')]),
            preserve_default=True,
        ),
    ]

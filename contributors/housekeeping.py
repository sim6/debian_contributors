# Debian Contributors maintenance functions
#
# Copyright (C) 2013--2014  Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals
import django_housekeeping as hk
from django.conf import settings
from contributors.models import User, Identifier
import logging
import os.path
import ldap
from . import models as cmodels

log = logging.getLogger(__name__)

DATA_DIR = getattr(settings, "DATA_DIR", None)
LDAP_DEBIAN_URI = getattr(settings, "LDAP_DEBIAN_URI", None)
LDAP_ALIOTH_URI = getattr(settings, "LDAP_ALIOTH_URI", None)

STAGES = ["harvest", "main", "association", "aggregation", "stats"]

class UserInfo(object):
    def __init__(self):
        # Map UID to { "fpr": fingerprint, "email": email }
        self.info = {}

    def add(self, dn, attrs, domain, email_field):
        if "uid" not in attrs:
            #print("Skipping entry with no uid:", dn)
            return
        uid = "{}@{}".format(attrs["uid"][0].decode("utf8"), domain)

        if domain == "users.alioth.debian.org":
            full_name = attrs["cn"][0].decode("utf8")
        else:
            full_name = attrs["gecos"][0].decode("utf8").strip(",")
        if full_name.isspace():
            full_name = ""

        if email_field not in attrs:
            #print("Entry with no {}: {}".format(email_field, dn))
            email = uid
        else:
            email = attrs[email_field][0].decode("utf8") if attrs[email_field] else uid
        self.info[uid] = { "fn": full_name, "email": email }
        # Optionally add fingerprint
        if "keyFingerPrint" in attrs:
            self.info[uid]["fpr"] = attrs["keyFingerPrint"][0]

    def add_debian(self, dn, attrs):
        self.add(dn, attrs, "debian.org", "emailForward")

    def add_alioth(self, dn, attrs):
        self.add(dn, attrs, "users.alioth.debian.org", "debGforgeForwardEmail")

    def add_identifier(self, ident):
        if ident.type == "email":
            if ident.name.endswith("@debian.org"):
                uid = ident.name[:-11]
                self.info[uid] = { "fn": ident.name, "email": ident.name }
            elif ident.name.endswith("@users.alioth.debian.org"):
                uid = ident.name[:-24]
                self.info[uid] = { "fn": ident.name, "email": ident.name }
        elif ident.type == "login":
            if ident.name.endswith("-guest"):
                email = ident.name + "@users.alioth.debian.org"
                self.info[ident.name] = { "fn": email, "email": email }
            else:
                email = ident.name + "@debian.org"
                self.info[ident.name] = { "fn": email, "email": email }

class UserAccountInfo(hk.Task):
    NAME = "user_account_info"

    def run_harvest(self, stage):
        userinfo = UserInfo()

        if getattr(settings, "DEVEL_SYSTEM", False):
            for i in Identifier.objects.all():
                userinfo.add_identifier(i)
        else:
            # Does not seem to be needed in Jessie anymore
            #ldap.set_option(ldap.OPT_X_TLS_CACERTFILE, "/etc/ssl/certs/ca-certificates.crt")

            # Get Debian usernames
            if LDAP_DEBIAN_URI is not None:
                l = ldap.initialize(LDAP_DEBIAN_URI)
                for dn, attrs in l.search_s("dc=debian,dc=org", ldap.SCOPE_SUBTREE, '(&(objectclass=inetOrgPerson)(gidNumber=800)(keyFingerPrint=*))', [b"uid", b"gecos", b"emailForward", b"keyFingerPrint"]):
                    userinfo.add_debian(dn, attrs)

            # Get Alioth usernames
            if LDAP_ALIOTH_URI is not None:
                l = ldap.initialize(LDAP_ALIOTH_URI)

                for dn, attrs in l.search_s("ou=Users,dc=alioth,dc=debian,dc=org", ldap.SCOPE_SUBTREE, attrlist=[b"uid", b"cn", b"debGforgeForwardEmail"]):
                    userinfo.add_alioth(dn, attrs)

            log.info("%d users loaded from Debian and Alioth's LDAP databases", len(userinfo.info))

        self.info = userinfo.info


class AssociateIdentities(hk.Task):
    """
    Machinery to associate identifiers to uids
    """
    NAME = "associate_identity"

    def __init__(self, *args, **kw):
        super(AssociateIdentities, self).__init__(*args, **kw)
        # Initialise counters for statistics
        self.assoc_users_reused = 0
        self.assoc_users_created = 0
        self.assoc_associated = 0

    def run_stats(self, stage):
        log.info("%s: users_reused: %d", self.IDENTIFIER, self.assoc_users_reused)
        log.info("%s: users_created: %d", self.IDENTIFIER, self.assoc_users_created)
        log.info("%s: users_associated: %d", self.IDENTIFIER, self.assoc_associated)

    def __call__(self, identity, uid, reason=None):
        """
        Called by maintenance tasks as self.hk.associate_identity
        """
        dry_run = self.hk.dry_run

        try:
            identity.user = User.objects.get(email=uid)
            self.assoc_users_reused += 1
        except User.DoesNotExist:
            # Infer an email address
            if uid.endswith("-guest"):
                email = uid + "@users.alioth.debian.org"
            else:
                email = uid + "@debian.org"
            if not dry_run: identity.user = User.objects.create_user(uid, email)
            self.assoc_users_created += 1
        if not dry_run:
            identity.save()
            if reason is None:
                identity.add_log("auto-associated to {}".format(uid))
            else:
                identity.add_log("auto-associated to {} {}".format(uid, reason))
        self.assoc_associated += 1

class AssociateLoginsInEmails(hk.Task):
    """
    Maintains User accounts on the application for Debian logins and alioth
    -guest logins
    """
    DEPENDS = [AssociateIdentities, UserAccountInfo]

    def __init__(self, *args, **kw):
        super(AssociateLoginsInEmails, self).__init__(*args, **kw)
        self.fullnames_filled = 0

    def run_association(self, stage):
        """
        Automatically make some default, safe associations
        """
        userinfo = self.hk.user_account_info.info

        # Map emails to uids
        by_email = {}
        for uid, info in userinfo.iteritems():
            email = info["email"]
            if not email: continue
            old_uid = by_email.get(email, None)
            if old_uid is None:
                by_email[email] = uid
            elif old_uid.endswith("@users.alioth.debian.org") and uid.endswith("@debian.org"):
                by_email[email] = uid
            elif old_uid.endswith("@debian.org") and uid.endswith("@users.alioth.debian.org"):
                pass
            else:
                #print("CONFLICT", email, uid, old_uid)
                pass

        # Map login names to User UIDs
        for i in cmodels.Identifier.objects.filter(type="login", user__isnull=True):
            # Ensure we only work on valid uids
            found = False
            for domain in "@debian.org", "@users.alioth.debian.org":
                uid = i.name + domain
                info = userinfo.get(uid, None)
                if info is not None:
                    found = True
            if not found: continue

            # Associate the user
            self.hk.associate_identity(i, uid)

        # Map emails to User UIDs
        for i in cmodels.Identifier.objects.filter(type="email", user__isnull=True):
            # If the email maps directly to a Debian or Alioth uid, we are done
            if i.name in userinfo:
                self.hk.associate_identity(i, i.name)
                continue

            # Else, lookup in forward emails
            uid = by_email.get(i.name, None)
            if uid is None: continue

            # Associate the user
            self.hk.associate_identity(i, uid)

        # Map unmapped fingerprints to debian accounts
        # Build a blacklist of mapped fingerprints
        mapped_fprs = set()
        for f in cmodels.Identifier.objects.filter(type="fpr", user__isnull=False):
            mapped_fprs.add(i.name)
        for uid, info in userinfo.iteritems():
            fpr = info.get("fpr", None)
            if fpr is None or fpr in mapped_fprs: continue

            # Create the identifier if it does not exist yet
            ident, created = cmodels.Identifier.objects.get_or_create(type="fpr", name=fpr)

            # Associate the user
            self.hk.associate_identity(ident, uid, reason="fingerprint found in Debian's LDAP")

        # Try to fill in missing full_name fields
        for u in cmodels.User.objects.filter(full_name=""):
            info = userinfo.get(u.email, None)
            if info is None: continue
            # FIXME: temporarily skip alioth full names with bad encodings
            if "?" in info["fn"]: continue
            if u.full_name != info["fn"]:
                self.fullnames_filled += 1
                u.add_log("full name set to {} from Debian/Alioth account information".format(info["fn"]))
                u.full_name = info["fn"]
                u.save()

    def log_stats(self):
        log.info("%s: full_name fields filled: %d", self.IDENTIFIER, self.fullnames_filled)

class AggregateContributors(hk.Task):
    """
    Recompute the Aggregated tables
    """
    def run_aggregation(self, stage):
        cmodels.AggregatedPersonContribution.recompute()
        cmodels.AggregatedSource.recompute()
        cmodels.AggregatedPerson.recompute()

class FixFullNames(hk.Task):
    """
    Recompute the Aggregated tables
    """
    def run_main(self, stage):
        for u in User.objects.all():
            if u.full_name.isspace():
                u.add_log("full name for {} was only made of spaces: setting it to the empty string".format(u.email))
                u.full_name = ""
                u.save()

# coding: utf8
# Debian Contributors website backend, data importer
#
# Copyright (C) 2013  Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals
from contributors import models
from debiancontributors import parser
from django import http
from django.utils.timezone import now
import json
import logging
import os.path

log = logging.getLogger(__name__)

class Results(object):
    def __init__(self):
        self.code = 200
        self.identifiers_skipped = 0
        self.contributions_processed = 0
        self.contributions_created = 0
        self.contributions_updated = 0
        self.contributions_skipped = 0
        self.records_parsed = 0
        self.records_skipped = 0
        self.errors = []

    def record_fail(self, e):
        self.code = e.code
        errors = getattr(e, "errors", None)
        if errors is not None:
            self.errors.extend(errors)
        else:
            self.errors.append(e.msg)

    def error(self, code, msg):
        self.code = code
        self.errors.append(msg)
        raise parser.Fail(self)

    def to_jsonable(self):
        return {
            "code": self.code,
            "errors": self.errors,
            "identifiers_skipped": self.identifiers_skipped,
            "contributions_processed": self.contributions_processed,
            "contributions_created": self.contributions_created,
            "contributions_updated": self.contributions_updated,
            "contributions_skipped": self.contributions_skipped,
            "records_parsed": self.records_parsed,
            "records_skipped": self.records_skipped,
        }

    def to_response(self):
        response = http.HttpResponse(content_type="text/plain", status=self.code)
        json.dump(self.to_jsonable(), response, indent=2)
        return response

    def test_dump(self, out=None):
        import sys
        if out is None:
            out = sys.stdout

        print("Results.code:", self.code, file=out)
        print("Results.identifiers_skipped:", self.identifiers_skipped, file=out)
        print("Results.contributions_processed:", self.contributions_processed, file=out)
        print("Results.contributions_created:", self.contributions_created, file=out)
        print("Results.contributions_updated:", self.contributions_updated, file=out)
        print("Results.contributions_skipped:", self.contributions_skipped, file=out)
        print("Results.records_parsed:", self.records_parsed, file=out)
        print("Results.records_skipped:", self.records_skipped, file=out)
        for e in self.errors:
            print("Results.errors:", e, file=out)

def get_source(name):
    """
    Get the importer.Source object for a Source with this name
    """
    try:
        return models.Source.objects.get(name=name)
    except models.Source.DoesNotExist:
        raise parser.Fail(404, "source '{}' not found".format(name))


def validate_auth_token(source, auth_token):
    if source.auth_token != auth_token:
        raise parser.Fail(403, "Authentication token is not correct")

class SourceInfo(object):
    """
    Prefetched cache of contribution types for a source
    """
    def __init__(self, source):
        self.source = source
        self.types = {}

        # Preload contribution types
        for ct in models.ContributionType.objects.filter(source=source):
            self.types[ct.name] = ct
        if not self.types:
            raise parser.Fail(404, "Source {} has no configured contribution types".format(source.name))

    def get(self, name):
        res = self.types.get(name, None)
        if res is None:
            raise parser.Fail(404, "Contribution type {} for source {} not found".format(name, self.source.name))
        return res

class Importer(object):
    """
    Data import infrastructure

    Methods starting with import_* return True if something was imported, else
    False.

    Other methods return what they should, or raise Fail if some validation failed.
    """
    def __init__(self):
        self.results = Results()

    def import_contribution(self, sourceinfo, ident, contrib):
        """
        Validate and insert or update a contribution, extending its span if it
        already exists
        """

        ctype = sourceinfo.get(contrib.type)
        ident, created = models.Identifier.objects.get_or_create(type=ident.type, name=ident.id)

        cnew = models.Contribution(
            identifier=ident,
            type=ctype,
            begin=contrib.begin,
            until=contrib.end,
            url=contrib.url,
        )

        try:
            # Look for an existing contribution
            c = models.Contribution.objects.get(identifier=ident, type=cnew.type)
        except models.Contribution.DoesNotExist:
            cnew.save()
            self.results.contributions_created += 1
            return cnew

        changed = c.extend(cnew.begin, cnew.until)

        if c.url != cnew.url:
            c.url = cnew.url
            changed = True

        if changed:
            self.results.contributions_updated += 1
            c.save()

        return c

    def import_request(self, request):
        """
        Validate and import data from a request
        """
        source = None
        parsed = None
        latest_entry = None
        try:
            # We only support POST
            if request.method != "POST":
                raise parser.Fail(400, "only POST requests are accepted")

            # Validate the source
            source = get_source(parser.get_key_string(request.POST, "source"))
            sourceinfo = SourceInfo(source)

            # Validate the auth token
            validate_auth_token(source, parser.get_key_string(request.POST, "auth_token"))

            # Validate the data
            data = request.FILES.get("data", None)
            if data is None:
                raise parser.Fail(400, "no file was posted in a 'data' field")

            # Detect compression type guessing it on uploaded file name
            file_ext = os.path.splitext(getattr(data, 'name', ''))[1]
            guess = file_ext.replace(os.path.extsep, '') or True
            compression = parser.get_key_string(request.POST, "data_compression", guess)

            # Decode the data
            parsed = parser.get_json(data, compression)

            # Import the data
            dcparser = parser.Parser()
            for ids, contribs in dcparser.parse_submission(parsed):
                self.results.records_parsed += 1
                for i in ids:
                    for c in contribs:
                        try:
                            # Track the highest contribution time
                            c = self.import_contribution(sourceinfo, i, c)
                            if latest_entry is None:
                                latest_entry = c.until
                            elif c.until is not None and c.until > latest_entry:
                                latest_entry = c.until
                            self.results.contributions_processed += 1
                        except parser.Fail as f:
                            self.results.record_fail(f)
            has_failures = False
        except parser.Fail as e:
            self.results.record_fail(e)
            has_failures = True

        # Log the submission
        if source and parsed:
            source.log_submission(request, parsed, self.results.to_jsonable())

        if source:
            # Update the health indicators
            source.last_import = now()
            if latest_entry is not None:
                source.last_contribution = latest_entry
            source.save()
            for ctype in source.contribution_types.all():
                models.AggregatedPersonContribution.recompute(ctype=ctype)
            models.AggregatedSource.recompute(source=source)
            models.AggregatedPerson.recompute()
        return not has_failures


class SourceImporter(object):
    """
    Source description import infrastructure

    Methods starting with import_* return True if something was imported, else
    False.

    Other methods return what they should, or raise Fail if some validation failed.
    """
    def __init__(self):
        self.results = Results()

    def get_contribution_type(self, source, data):
        if not isinstance(data, dict):
            raise parser.Fail(400, "contribution type data is not an object")

        defaults = {
            "desc": parser.get_key_unicode(data, "desc"),
            "contrib_desc": parser.get_key_unicode(data, "contrib_desc"),
        }

        ct, created = models.ContributionType.objects.get_or_create(
            source=source,
            name=parser.get_key_string(data, "name"),
            defaults=defaults)
        if not created:
            ct.desc = defaults["desc"]
            ct.contrib_desc = defaults["contrib_desc"]
            ct.save()

        return ct

    def get_source(self, data):
        """
        Validate and import one source from a dict
        """
        if not isinstance(data, dict):
            raise parser.Fail(400, "source data is not an object")

        defaults = {
            "desc": parser.get_key_unicode(data, "desc"),
            "url": parser.get_key_unicode(data, "url"),
            "auth_token": parser.get_key_string(data, "auth_token"),
        }

        ctypes = parser.get_key_sequence_or_object(data, "contribution_types")

        s, created = models.Source.objects.get_or_create(
            name=parser.get_key_string(data, "name"),
            defaults=defaults)
        if not created:
            s.desc = defaults["desc"]
            s.url = defaults["url"]
            s.auth_token = defaults["auth_token"]
            s.save()

        old_ctypes = { ct.pk: ct for ct in s.contribution_types.all() }

        for c in ctypes:
            ct = self.get_contribution_type(s, c)
            old_ctypes.pop(ct.pk, None)

        # Delete old ones
        for ct in old_ctypes.iteritems():
            ct.delete()

    def import_sources(self, data):
        """
        Validate and import sources from a list of exported sources
        """
        try:
            if not isinstance(data, (list, tuple)):
                raise parser.Fail(400, "Sources data is not a list")

            for d in data:
                self.get_source(d)

            return True
        except parser.Fail as e:
            return False

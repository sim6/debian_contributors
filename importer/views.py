# coding: utf8
# Debian Contributors import/export and basic website functions
#
# Copyright (C) 2013--2014  Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals
from django.utils.translation import ugettext as _
from django.views.decorators.csrf import csrf_exempt
from django import http
from .importer import Importer
import contributors.models as cmodels
import json

@csrf_exempt
def post_source(request):
    if request.method != "POST":
        return http.HttpResponseBadRequest(_("Only POST method is allowed"))
    # Import the data
    importer = Importer()
    importer.import_request(request)
    return importer.results.to_response()

def export_sources(request):
    if request.user.is_anonymous() or not request.user.is_superuser:
        data = cmodels.Source.export_json()
    else:
        data = cmodels.Source.export_json(with_tokens=True)

    res = http.HttpResponse(content_type="application/json")
    json.dump(data, res, indent=2)
    return res


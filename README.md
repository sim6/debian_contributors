Debian Contributors web application
===================================

See https://wiki.debian.org/Teams/FrontDesk/DcSiteDevel for this project's page in the Debian wiki.

## Running this code on your own machine

### Dependencies
    
    apt-get install python-django python-ldap python-debiancontributors eatmydata
      (eatmydata is not necessary but it will make everything run much faster 
       in a test environment)

    # https://github.com/spanezz/django-housekeeping
    git clone https://github.com/spanezz/django-housekeeping
      (you can either build the package from it or symlink the module directory
      into the contributors.debian.org sources)

### Configuration

    cp dc/local_settings.py.devel dc/local_settings.py
    edit dc/local_settings.py as needed

### First setup

    ./manage.py migrate

You may wish to add an admin user that will be named the same as the DACS_TEST_USERNAME (see Authentication below).

### Fill in data

Import source information from http://contributors.debian.org/contributors/export/sources
if you are not logged as a DD, authentication tokens are replaced with dummy
ones:

    curl https://contributors.debian.org/contributors/export/sources | eatmydata ./manage.py import_sources

    eatmydata ./manage.py runserver

From now on, imported sources will be available from http://localhost:8000/sources/. 

You may login to http://localhost:8000/admin/ which grants access to the sources and other tables. (currently broken)

Go to https://contributors.debian.org/sources/, choose a data source, click on
"Members", then "Add me as member": you'll then be able to access the data
source configuration. From there you can see the data source name and the auth
token for posting.

Acquire some JSON data (ask Enrico at the moment) and import it:

    dc-tool --post --baseurl=http://localhost:8000 --source sourcename --auth-token sourcetoken datafile.json

If you're a DD or have an account on Alioth, you can fetch the website contribs data with:

    scp git.debian.org:~enrico/www-dc.json .

And import it with (the token is available from the admin interface if you configured the source accordingly):
    
    dc-tool --post --baseurl=http://localhost:8000 www.debian.org whatevertokenisneeded www-dc.json


Sync Debian keyrings into data/keyrings/ :

    ./synckeyrings.sh

    eatmydata ./manage.py maintcarnivore

### Run housekeeping

Update the alioth and debian UIDs, and create Django user accounts accordingly:

    # ssh alioth.debian.org -L 3389:localhost:389
    eatmydata ./manage.py housekeeping

### Run the web server
    
    eatmydata ./manage.py runserver

## Development

Development targets the current Debian Stable plus backports, and that
determines the version of Django and the dependencies that can be used.

You can find a TODO list at https://wiki.debian.org/Teams/FrontDesk/DcSiteDevel

## Authentication

Authentication in production is meant to be performed against DACS, the Debian SSO system.
The django_dacs/ authenticator generates Django authentication from the REMOTE_USER 
variable retrieved from DACS.
For testing purposes, when not on a debian.org machine allowed to query sso.debian.org, 
one may set the DACS_TEST_USERNAME config to specify which user name to use.


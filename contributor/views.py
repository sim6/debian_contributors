# Debian Contributors website backend
#
# Copyright (C) 2013--2014  Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django import http
from django.http import Http404
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, get_object_or_404
from django.views.generic import DetailView, TemplateView
from django.core.exceptions import PermissionDenied
from contributors import models as cmodels
from django.utils.translation import ugettext as _

class ContributorView(DetailView):
    template_name = "contributor/contributor.html"
    context_object_name='contributor'

    def get_object(self):
        name = self.kwargs["name"]
        if name.endswith("@debian"):
            name += ".org"
        elif name.endswith("@alioth"):
            name = name[:-6] + "users.alioth.debian.org"
        # This will be set if the user was referred by identifier instead of by
        # username
        ident = None
        try:
            obj = cmodels.User.objects.get(email=name)
        except cmodels.User.DoesNotExist:
            try:
                ident = cmodels.Identifier.get_auto(name)
                try:
                    obj = ident.user
                except cmodels.User.DoesNotExist:
                    obj = None
            except ValueError:
                obj = None
            except cmodels.Identifier.DoesNotExist:
                obj = None

        if obj is not None and not (self.request.user.is_superuser or (self.request.user.is_authenticated() and self.request.user.pk == obj.pk)):
            # Someone else is visting this user: enforce visibility settings
            if obj.hidden or (ident and ident.hidden):
                obj = None

        if obj is None:
            raise Http404(_("No contributor found matching the query"))

        return obj

    def get_context_data(self, **kwargs):
        context = super(ContributorView, self).get_context_data(**kwargs)
        if not self.request.user.is_authenticated():
            own_page = False
        else:
            own_page = self.request.user.is_superuser or self.request.user.pk == self.object.pk

        if own_page:
            context["user_log"] = list(self.object.log.order_by("-ts"))
            source_settings = cmodels.UserSourceSettings.objects.filter(user=self.object, hidden=True).distinct()
            identifiers = []
            for ident in self.object.identifiers.order_by("type", "name"):
                identifiers.append({
                    "ident": ident,
                    "log": ident.log.order_by("-ts"),
                })
            context["identifiers"] = identifiers
            context["source_settings"] = list(source_settings)
            context["hide_user"] = self.object.hidden

        contributions = []
        for c in self.object.contributions().order_by("identifier"):
            if own_page or not c.hidden:
                contributions.append(c)

        context["contributions"] = contributions

        context['redacted'] = not own_page
        return context

@login_required
def save_settings(request, name):
    if request.method != "POST":
        return http.HttpResponseBadRequest(_("Only POST method is allowed"))
    user = get_object_or_404(cmodels.User, email=name)

    changed = False

    # Updated user info
    if request.POST.get("full_name", "") != user.full_name:
        user.full_name = request.POST.get("full_name", "")
        user.save()

    # Update user visibility
    want_hidden_user = bool(request.POST.get("hide_user", False))
    if user.hidden != want_hidden_user:
        user.hidden = want_hidden_user
        user.save()
        changed = True

    # Update identifier visibility
    for ident in user.identifiers.all():
        want_hidden = bool(request.POST.get("hide_{}".format(ident.pk), False))
        if ident.hidden != want_hidden:
            ident.hidden = want_hidden
            ident.save()
            changed = True

    if changed:
        cmodels.AggregatedPersonContribution.recompute(user=request.user)
        cmodels.AggregatedSource.recompute()
        cmodels.AggregatedPerson.recompute(user=request.user)

    return redirect("contributor_detail", name=user.email)

@login_required
def impersonate(request, name=None):
    if "impersonate" in request.session:
        del request.session["impersonate"]

    if request.user.is_superuser and name != request.user.email:
        try:
            new_user = cmodels.User.objects.get(email=name)
            request.session["impersonate"] = new_user.email
        except cmodels.User.DoesNotExist:
            pass

    url = request.GET.get("url", None)
    if url is None:
        return redirect('contributors')
    else:
        return redirect(url)

class Unclaim(TemplateView):
    template_name = "contributor/unclaim.html"

    def get_person(self, request, email):
        person = get_object_or_404(cmodels.User, email=email)

        if not request.user.is_authenticated() or (
                not request.user.is_superuser
                and not request.user.is_dd
                and request.user.pk != person.pk):
            raise PermissionDenied

        return person

    def get_context_data(self, **kw):
        res = super(Unclaim, self).get_context_data(**kw)

        person = self.get_person(self.request, self.kwargs["name"])
        own_page = self.request.user.pk == person.pk

        res.update(
            contributor=person,
            identifiers=person.identifiers.order_by("type", "name"),
            own_page=own_page,
        )
        return res

    def post(self, request, name):
        person = self.get_person(request, name)
        ident = get_object_or_404(cmodels.Identifier, pk=request.POST["id"])

        if ident.user is None:
            return redirect("contributor_detail", name=person.email)

        if ident.user.pk != person.pk:
            raise PermissionDenied

        ident.user = None
        ident.save()
        log_entry = "Unassociated from {} by {} using the web interface".format(person.email, request.user.email)
        person.add_log(log_entry)
        ident.add_log(log_entry)

        return redirect("contributor_detail", name=person.email)

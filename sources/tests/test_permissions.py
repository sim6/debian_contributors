# coding: utf8
# Debian Contributors unit tests
#
# Copyright (C) 2014  Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# Create your views here.

from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals
from django.test import TestCase
from contributors import models as cmodels
from sources.test_common import *

class SourceTestClient(DCTestClient):
    def setup_query(self, member=False, **kw):
        super(SourceTestClient, self).setup_query(member=member, **kw)
        self.fixture.source.admins.clear()
        if self.user and member:
            self.fixture.source.admins.add(self.user)

class ThenSuccessCountVerified(DCTestUtilsThen):
    def __call__(self, fixture, response, when, test_client):
        if response.status_code != 302:
            fixture.fail("User {} got status code {} instead of a Redirect when {}".format(
                when.user, response.status_code, when))

        for k, v in getattr(when, 'count_map', {}).iteritems():
            c = getattr(when, "count_{}".format(k), lambda: -1)()
            if v != c:
                fixture.fail("Expected count for {} disappointed: {} != {}".format(k, v, c))

class WhenDeleteContributions(DCTestUtilsWhen):
    method = "post"

    def setUp(self, fixture):
        super(WhenDeleteContributions, self).setUp(fixture)
        self.source = cmodels.Source(name="newsource", desc="newdesc", auth_token="newtoken")
        self.source.save()
        m_name = self.args.get("member_name", None)
        if isinstance(m_name, basestring):
            self.source.admins.add(getattr(fixture, "user_{}".format(m_name)))

        self.ctype1 = cmodels.ContributionType(name="newctype1", desc="newdesc",
                                               contrib_desc="c1desc", source=self.source)
        self.ctype1.save()

        cmodels.Contribution.objects.create(type=self.ctype1, identifier=fixture.user_dd1.identifiers.first(),
                                            begin=cmodels.Contribution.objects.first().begin,
                                            until=cmodels.Contribution.objects.first().until)

        self.ctype2 = cmodels.ContributionType(name="newctype2", desc="newdesc",
                                               contrib_desc="c2desc", source=self.source)
        self.ctype2.save()

        cmodels.Contribution.objects.create(type=self.ctype2, identifier=fixture.user_alioth1.identifiers.first(),
                                            begin=cmodels.Contribution.objects.first().begin,
                                            until=cmodels.Contribution.objects.first().until)

        cmodels.AggregatedPersonContribution.recompute(ctype=self.ctype1)
        cmodels.AggregatedPersonContribution.recompute(ctype=self.ctype2)
        cmodels.AggregatedSource.recompute(source=self.source)

    def count_source_contributions(self):
        return self.ctype1.contributions.count() + \
            self.ctype2.contributions.count()

    def count_aggregated_source(self):
        return cmodels.AggregatedSource.objects.filter(source=self.source).count()

    def count_aggregated_person_contrib(self):
        return cmodels.AggregatedPersonContribution.objects.filter(ctype=self.ctype1).count() + \
            cmodels.AggregatedPersonContribution.objects.filter(ctype=self.ctype2).count()

    def tearDown(self, fixture):
        super(WhenDeleteContributions, self).tearDown(fixture)
        self.source.delete()

    def __unicode__(self):
        return "deleting contributions at {}".format(self.url)


class SourcesTestCase(SimpleSourceFixtureMixin, DCTestUtilsMixin, TestCase):
    def test_source_list(self):
        tc = SourceTestClient(self, 'source_list')
        for user in (self.user_admin, self.user_dd, self.user_alioth, None):
            tc.assertGet(Success(), user)

    def test_source_view(self):
        tc = SourceTestClient(self, 'source_view', url_kwargs={"sname": "test"})
        for user in (self.user_admin, self.user_dd, self.user_alioth, None):
            tc.assertGet(Success(), user)

    def test_source_add(self):
        class WhenViewAddSourceForm(DCTestUtilsWhen):
            url = reverse("source_add")

        class WhenPostIncompleteAddSourceForm(DCTestUtilsWhen):
            url = reverse("source_add")
            method = "post"
            def __unicode__(self):
                return "user {} posts an incomplete form to {}".format(self.user, self.url)

        class WhenSubmitAddSourceForm(DCTestUtilsWhen):
            url = reverse("source_add")
            method = "post"
            data = {
                "name": "newsource",
                "desc": "new test source",
                "url": "http://www.example.org/new",
                "auth_token": "newtoken",
            }

            def setUp(self, fixture):
                super(WhenSubmitAddSourceForm, self).setUp(fixture)
                cmodels.Source.objects.filter(name="newsource").delete()

            def tearDown(self, fixture):
                super(WhenSubmitAddSourceForm, self).tearDown(fixture)
                cmodels.Source.objects.filter(name="newsource").delete()

            def __unicode__(self):
                return "user {} adds a new data source at {}".format(self.user, self.url)

        class ThenCanAdd(DCTestUtilsThen):
            def __call__(self, fixture, response, when, test_client):
                fixture.assertEquals(response.status_code, 302)
                fixture.assertEquals(response["Location"], "http://testserver/sources/")
                try:
                    s = cmodels.Source.objects.get(name="newsource")
                except cmodels.Source.DoesNotExist:
                    s = None
                fixture.assertIsNotNone(s)
                fixture.assertEquals(s.desc, when.data["desc"])
                fixture.assertEquals(s.url, when.data["url"])
                fixture.assertEquals(s.auth_token, when.data["auth_token"])
                fixture.assert_(s.admins.filter(pk=when.user.pk).exists())

        class ThenCannotAdd(ThenForbidden):
            def __call__(self, fixture, response, when, test_client):
                super(ThenCannotAdd, self).__call__(fixture, response, when, test_client)
                try:
                    s = cmodels.Source.objects.get(name="newsource")
                except cmodels.Source.DoesNotExist:
                    s = None
                fixture.assertIsNone(s)

        self.assertVisit(WhenViewAddSourceForm(user=self.user_admin), ThenSuccess())
        self.assertVisit(WhenViewAddSourceForm(user=self.user_dd), ThenSuccess())
        self.assertVisit(WhenViewAddSourceForm(user=self.user_alioth), ThenForbidden())
        self.assertVisit(WhenViewAddSourceForm(user=None), ThenForbidden())

        self.assertVisit(WhenPostIncompleteAddSourceForm(user=self.user_admin), ThenSuccess())
        self.assertVisit(WhenPostIncompleteAddSourceForm(user=self.user_dd), ThenSuccess())
        self.assertVisit(WhenPostIncompleteAddSourceForm(user=self.user_alioth), ThenCannotAdd())
        self.assertVisit(WhenPostIncompleteAddSourceForm(user=None), ThenCannotAdd())

        self.assertVisit(WhenSubmitAddSourceForm(user=self.user_admin), ThenCanAdd())
        self.assertVisit(WhenSubmitAddSourceForm(user=self.user_dd), ThenCanAdd())
        self.assertVisit(WhenSubmitAddSourceForm(user=self.user_alioth), ThenCannotAdd())
        self.assertVisit(WhenSubmitAddSourceForm(user=None), ThenCannotAdd())

    def test_source_update(self):
        class WhenViewForm(DCTestUtilsWhen):
            url = reverse("source_update", kwargs={"sname": "test"})
            ORIG_DESC = "test source"
            ORIG_URL = "http://www.example.org"
            ORIG_TOKEN = "testsecret"
            def _restore_source(self):
                s = cmodels.Source.objects.get(name="test")
                s.desc = self.ORIG_DESC
                s.url = self.ORIG_URL
                s.auth_token = self.ORIG_TOKEN
                s.save()
            def setUp(self, fixture):
                super(WhenViewForm, self).setUp(fixture)
                if self.user and self.args.get("member", False):
                    fixture.source.admins.add(self.user)
                self._restore_source()
            def tearDown(self, fixture):
                super(WhenViewForm, self).tearDown(fixture)
                fixture.source.admins.clear()
                self._restore_source()
        class WhenPostIncompleteForm(WhenViewForm):
            method = "post"
            def __unicode__(self):
                return "user {} posts an incomplete form to {}".format(self.user, self.url)
        class WhenPostForm(WhenViewForm):
            method = "post"
            data = {
                "name": "test",
                "desc": "new test source",
                "url": "http://www.example.org/new",
                "auth_token": "newtestsecret",
            }
            def __unicode__(self):
                return "user {} updates the data source at {}".format(self.user, self.url)
        class ThenCanUpdate(DCTestUtilsThen):
            def __call__(self, fixture, response, when, test_client):
                fixture.assertEquals(response.status_code, 302)
                fixture.assertEquals(response["Location"], "http://testserver/sources/")
                try:
                    s = cmodels.Source.objects.get(name="test")
                except cmodels.Source.DoesNotExist:
                    s = None
                fixture.assertIsNotNone(s)
                fixture.assertEquals(s.desc, when.data["desc"])
                fixture.assertEquals(s.url, when.data["url"])
                fixture.assertEquals(s.auth_token, when.data["auth_token"])
        class ThenCannotUpdate(ThenForbidden):
            def __call__(self, fixture, response, when, test_client):
                super(ThenCannotUpdate, self).__call__(fixture, response, when, test_client)
                try:
                    s = cmodels.Source.objects.get(name="test")
                except cmodels.Source.DoesNotExist:
                    s = None
                fixture.assertIsNotNone(s)
                fixture.assertEquals(s.desc, when.ORIG_DESC)
                fixture.assertEquals(s.url, when.ORIG_URL)
                fixture.assertEquals(s.auth_token, when.ORIG_TOKEN)

        # GETs
        self.assertVisit(WhenViewForm(user=self.user_admin), ThenSuccess())
        self.assertVisit(WhenViewForm(user=self.user_dd), ThenForbidden())
        self.assertVisit(WhenViewForm(user=self.user_alioth), ThenForbidden())
        self.assertVisit(WhenViewForm(user=None), ThenForbidden())
        for user in (self.user_admin, self.user_dd, self.user_alioth):
            self.assertVisit(WhenViewForm(user=user, member=True), ThenSuccess())

        # Empty posts (load form with validation messages)
        self.assertVisit(WhenPostIncompleteForm(user=self.user_admin), ThenSuccess())
        self.assertVisit(WhenPostIncompleteForm(user=self.user_dd), ThenCannotUpdate())
        self.assertVisit(WhenPostIncompleteForm(user=self.user_alioth), ThenCannotUpdate())
        self.assertVisit(WhenPostIncompleteForm(user=None), ThenCannotUpdate())
        for user in (self.user_admin, self.user_dd, self.user_alioth):
            self.assertVisit(WhenPostIncompleteForm(user=user, member=True), ThenSuccess())

        # All members can update a data source
        for user in (self.user_admin, self.user_dd, self.user_alioth):
            self.assertVisit(WhenPostForm(user=user, member=True), ThenCanUpdate())

        # Among non-members, only superuser can update a data source
        self.assertVisit(WhenPostForm(user=self.user_admin), ThenCanUpdate())
        for user in (self.user_dd, self.user_alioth, None):
            self.assertVisit(WhenPostForm(user=user), ThenCannotUpdate())

    def test_source_delete(self):
        class WhenSourceDeleteBase(DCTestUtilsWhen):
            url = reverse("source_delete", kwargs={"sname": "newsource"})

            def setUp(self, fixture):
                super(WhenSourceDeleteBase, self).setUp(fixture)
                cmodels.Source.objects.filter(name="newsource").delete()
                self.source = cmodels.Source.objects.create(name="newsource", desc="newdesc", auth_token="newtoken")
                if self.user and self.args.get("member", False):
                    self.source.admins.add(self.user)

            def tearDown(self, fixture):
                super(WhenSourceDeleteBase, self).tearDown(fixture)
                self.source.delete()

        class WhenViewSourceDeleteForm(WhenSourceDeleteBase):
            pass

        class WhenSubmitDeleteForm(WhenSourceDeleteBase):
            method = "post"
            def __unicode__(self):
                return "user {} deletes a data source at {}".format(self.user, self.url)

        class ThenCanDelete(DCTestUtilsThen):
            def __call__(self, fixture, response, when, test_client):
                fixture.assertEquals(response.status_code, 302)
                fixture.assertEquals(response["Location"], "http://testserver/sources/")
                try:
                    s = cmodels.Source.objects.get(name="newsource")
                except cmodels.Source.DoesNotExist:
                    s = None
                fixture.assertIsNone(s)

        class ThenCannotDelete(ThenForbidden):
            def __call__(self, fixture, response, when, test_client):
                super(ThenCannotDelete, self).__call__(fixture, response, when, test_client)
                try:
                    s = cmodels.Source.objects.get(name="newsource")
                except cmodels.Source.DoesNotExist:
                    s = None
                fixture.assertIsNotNone(s)

        # GETs
        self.assertVisit(WhenViewSourceDeleteForm(user=self.user_admin), ThenSuccess())
        self.assertVisit(WhenViewSourceDeleteForm(user=self.user_dd), ThenForbidden())
        self.assertVisit(WhenViewSourceDeleteForm(user=self.user_alioth), ThenForbidden())
        self.assertVisit(WhenViewSourceDeleteForm(user=None), ThenForbidden())
        for user in (self.user_admin, self.user_dd, self.user_alioth):
            self.assertVisit(WhenViewSourceDeleteForm(user=user, member=True), ThenSuccess())

        # All members can update a data source
        for user in (self.user_admin, self.user_dd, self.user_alioth):
            self.assertVisit(WhenSubmitDeleteForm(user=user, member=True), ThenCanDelete())

        # Among non-members, only superuser can update a data source
        self.assertVisit(WhenSubmitDeleteForm(user=self.user_admin), ThenCanDelete())
        for user in (self.user_dd, self.user_alioth, None):
            self.assertVisit(WhenSubmitDeleteForm(user=user), ThenCannotDelete())

    def test_source_delete_contributions(self):
        class WhenDeleteSourceContributions(WhenDeleteContributions):
            def setUp(self, fixture):
                super(WhenDeleteSourceContributions, self).setUp(fixture)

                self.count_map = {
                    "source_contributions": 0,
                    "aggregated_source": 0,
                    "aggregated_person_contrib": 0,
                }
                self.url = reverse("source_delete_contributions", kwargs={ "sname": self.source.name })


        self.assertVisit(WhenDeleteSourceContributions(user="admin"), ThenSuccessCountVerified())
        for u in "dd", "dd1", "alioth", "alioth1":
            self.assertVisit(WhenDeleteSourceContributions(user=u), ThenForbidden())

        for u in "dd", "dd1", "alioth", "alioth1":
            self.assertVisit(WhenDeleteSourceContributions(user=u, member_name=u), ThenSuccessCountVerified())

    def test_source_members(self):
        tc = SourceTestClient(self, 'source_members', url_kwargs={"sname": "test"})

        # GETs
        tc.assertGet(Success(), self.user_admin)
        tc.assertGet(Success(), self.user_dd)
        tc.assertGet(NotFound(), self.user_alioth)
        tc.assertGet(NotFound(), None)
        for user in (self.user_admin, self.user_dd, self.user_alioth):
            tc.assertGet(Success(), user, member=True)

    def test_source_members_add(self):
        class WhenGetPage(DCTestUtilsWhen):
            url = reverse("source_members_add", kwargs={"sname": "test"})
            def setUp(self, fixture):
                super(WhenGetPage, self).setUp(fixture)
                fixture.source.admins.clear()
                if self.user and self.args.get("member", False):
                    fixture.source.admins.add(self.user)
            def tearDown(self, fixture):
                super(WhenGetPage, self).tearDown(fixture)
                fixture.source.admins.clear()

        # GETs
        for user in (self.user_admin, self.user_dd, self.user_alioth, None):
            self.assertVisit(WhenGetPage(user=user), ThenBadMethod())
        for user in (self.user_admin, self.user_dd, self.user_alioth):
            self.assertVisit(WhenGetPage(user=user, member=True), ThenBadMethod())


        # Empty posts
        class WhenPostEmpty(WhenGetPage):
            method = "post"
        for user in (self.user_admin, self.user_dd, self.user_alioth, None):
            self.assertVisit(WhenPostEmpty(user=user), ThenNotFound())
        for user in (self.user_admin, self.user_dd, self.user_alioth):
            self.assertVisit(WhenPostEmpty(user=user, member=True), ThenNotFound())

        # Posts with nonexisting people
        class WhenPostInvalid(WhenPostEmpty):
            data = { "name": "foo@debian.org" }
        for user in (self.user_admin, self.user_dd, self.user_alioth, None):
            self.assertVisit(WhenPostInvalid(user=user), ThenNotFound())
        for user in (self.user_admin, self.user_dd, self.user_alioth):
            self.assertVisit(WhenPostInvalid(user=user, member=True), ThenNotFound())


        class WhenPostValid(WhenPostEmpty):
            def setUp(self, fixture):
                super(WhenPostValid, self).setUp(fixture)
                self.new_user = self.args["new_user"]
                if isinstance(self.new_user, basestring):
                    self.new_user = getattr(fixture, "user_{}".format(self.new_user))
                self.data = { "name": self.new_user.email }

        class ThenCanAdd(DCTestUtilsThen):
            def __call__(self, fixture, response, when, test_client):
                fixture.assertEquals(response.status_code, 302)
                fixture.assertEquals(response["Location"], "http://testserver/source/test/members/")
                fixture.assertIn(when.new_user.email, [x.email for x in fixture.source.admins.order_by("email")])

        class ThenCannotAdd(DCTestUtilsThen):
            def __call__(self, fixture, response, when, test_client):
                fixture.assertEquals(response.status_code, 403)
                fixture.assertNotIn(when.new_user.email, [x.email for x in fixture.source.admins.order_by("email")])

        # Superuser can add anyone
        for user in (self.user_admin, self.user_dd, self.user_alioth):
            self.assertVisit(WhenPostValid(user=self.user_admin, new_user=user), ThenCanAdd())
            self.assertVisit(WhenPostValid(user=self.user_admin, new_user=user, member=True), ThenCanAdd())

        # Non-member DD can only add self
        self.assertVisit(WhenPostValid(user=self.user_dd, new_user=self.user_admin), ThenCannotAdd())
        self.assertVisit(WhenPostValid(user=self.user_dd, new_user=self.user_dd), ThenCanAdd())
        self.assertVisit(WhenPostValid(user=self.user_dd, new_user=self.user_dd1), ThenCannotAdd())
        self.assertVisit(WhenPostValid(user=self.user_dd, new_user=self.user_alioth), ThenCannotAdd())

        # Member DD can add anyone
        for user in (self.user_admin, self.user_dd, self.user_dd1, self.user_alioth):
            self.assertVisit(WhenPostValid(user=self.user_dd, new_user=user, member=True), ThenCanAdd())

        # Non-member alioth cannot add anyone
        for user in (self.user_admin, self.user_dd, self.user_alioth, self.user_alioth1):
            self.assertVisit(WhenPostValid(user=self.user_alioth, new_user=user), ThenCannotAdd())

        # Member alioth can add anyone
        for user in (self.user_admin, self.user_dd, self.user_alioth, self.user_alioth1):
            self.assertVisit(WhenPostValid(user=self.user_alioth, new_user=user, member=True), ThenCanAdd())

        # Anonymous cannot add anyone
        for user in (self.user_admin, self.user_dd, self.user_alioth):
            self.assertVisit(WhenPostValid(user=None, new_user=user), ThenCannotAdd())

    def test_source_members_delete(self):
        tc = SourceTestClient(self, "source_members_delete", url_kwargs={"sname": "test"})

        class DCTestClientDeleteUserCheck(DCTestClientCheck):
            def __init__(self, user):
                self.user = user
            def get_default_data(self, tc):
                return {"name": self.user.email}
            def setup_query(self, tc, **kw):
                tc.fixture.source.admins.add(self.user)

        class CanDelete(DCTestClientDeleteUserCheck):
            def check_result(self, tc):
                if tc.response.status_code != 302:
                    tc.fixture.fail("User {} got status code {} instead of a Redirect deleting {}".format(
                        tc.user, tc.response.status_code, self.user))
                tc.fixture.assertEquals(tc.response.status_code, 302)
                tc.fixture.assertEquals(tc.response["Location"], "http://testserver/source/test/members/")
                tc.fixture.assertNotIn(self.user.email, [x.email for x in tc.fixture.source.admins.order_by("email")])

        class CannotDelete(DCTestClientDeleteUserCheck):
            def check_result(self, tc):
                if tc.response.status_code != 403:
                    tc.fixture.fail("User {} got status code {} instead of a Forbidden deleting {}".format(
                        tc.user, tc.response.status_code, self.user))
                tc.fixture.assertEquals(tc.response.status_code, 403)
                tc.fixture.assertIn(self.user.email, [x.email for x in tc.fixture.source.admins.order_by("email")])

        # GETs
        for user in (self.user_admin, self.user_dd, self.user_alioth, None):
            tc.assertGet(BadMethod(), user)
        for user in (self.user_admin, self.user_dd, self.user_alioth):
            tc.assertGet(BadMethod(), user, member=True)

        # Empty posts
        for user in (self.user_admin, self.user_dd, self.user_alioth, None):
            tc.assertPost(NotFound(), user)
        for user in (self.user_admin, self.user_dd, self.user_alioth):
            tc.assertPost(NotFound(), user, member=True)

        # Posts with nonexisting people
        for user in (self.user_admin, self.user_dd, self.user_alioth, None):
            tc.assertPost(NotFound(), user, data={"name": "foo@debian.org"})
        for user in (self.user_admin, self.user_dd, self.user_alioth):
            tc.assertPost(NotFound(), user, data={"name": "foo@debian.org"}, member=True)

        # Superuser can delete anyone
        for user in (self.user_admin, self.user_dd, self.user_alioth):
            tc.assertPost(CanDelete(user), self.user_admin)
            tc.assertPost(CanDelete(user), self.user_admin, member=True)

        # Non-member DD can not delete anyone
        for user in (self.user_admin, self.user_dd1, self.user_alioth):
            tc.assertPost(CannotDelete(user), self.user_dd)

        # Member DD can delete anyone
        for user in (self.user_admin, self.user_dd, self.user_dd1, self.user_alioth):
            tc.assertPost(CanDelete(user), self.user_dd, member=True)

        # Non-member alioth cannot delete anyone
        for user in (self.user_admin, self.user_dd, self.user_alioth1):
            tc.assertPost(CannotDelete(user), self.user_alioth)

        # Member alioth can delete anyone
        for user in (self.user_admin, self.user_dd, self.user_alioth, self.user_alioth1):
            tc.assertPost(CanDelete(user), self.user_alioth, member=True)

        # Anonymous cannot delete anyone
        for user in (self.user_admin, self.user_dd, self.user_alioth):
            tc.assertPost(CannotDelete(user), None)

    def test_ctype_view(self):
        tc = SourceTestClient(self, 'source_ctype_view', url_kwargs={"sname": "test", "name": "tester"})
        for user in (self.user_admin, self.user_dd, self.user_alioth, None):
            tc.assertGet(Success(), user)
        for user in (self.user_admin, self.user_dd, self.user_alioth):
            tc.assertGet(Success(), user, member=True)

    def test_ctype_add(self):
        class WhenAddCtypeBase(DCTestUtilsWhen):
            url = reverse("source_ctype_add", kwargs={"sname": "test"})

            def setUp(self, fixture):
                super(WhenAddCtypeBase, self).setUp(fixture)
                fixture.source.admins.clear()
                if self.user and self.args.get("member", False):
                    fixture.source.admins.add(self.user)
                fixture.source.contribution_types.filter(name="umareller").delete()

            def tearDown(self, fixture):
                super(WhenAddCtypeBase, self).tearDown(fixture)
                fixture.source.admins.clear()
                fixture.source.contribution_types.filter(name="umareller").delete()

        class WhenViewAddCtypeForm(WhenAddCtypeBase):
            pass

        class WhenPostIncompleteAddCtypeForm(WhenAddCtypeBase):
            method = "post"
            def __unicode__(self):
                return "user {} posts an incomplete form to {}".format(self.user, self.url)

        class WhenSubmitAddCtypeForm(WhenAddCtypeBase):
            method = "post"
            data = {
                "name": "umareller",
                "desc": "udesc",
                "contrib_desc": "ucdesc",
            }
            def __unicode__(self):
                return "user {} adds a new contribution type at {}".format(self.user, self.url)

        class ThenCanAdd(DCTestUtilsThen):
            def __call__(self, fixture, response, when, test_client):
                fixture.assertEquals(response.status_code, 302)
                fixture.assertEquals(response["Location"], "http://testserver/source/test/update/")
                try:
                    ct = fixture.source.contribution_types.get(name="umareller")
                except cmodels.ContributionType.DoesNotExist:
                    ct = None
                fixture.assertIsNotNone(ct)
                fixture.assertEquals(ct.name, when.data["name"])
                fixture.assertEquals(ct.desc, when.data["desc"])
                fixture.assertEquals(ct.contrib_desc, when.data["contrib_desc"])

        class ThenCannotAdd(ThenForbidden):
            def __call__(self, fixture, response, when, test_client):
                super(ThenCannotAdd, self).__call__(fixture, response, when, test_client)
                try:
                    ct = fixture.source.contribution_types.get(name="umareller")
                except cmodels.ContributionType.DoesNotExist:
                    ct = None
                fixture.assertIsNone(ct)

        # GETs
        self.assertVisit(WhenViewAddCtypeForm(user=self.user_admin), ThenSuccess())
        self.assertVisit(WhenViewAddCtypeForm(user=self.user_dd), ThenForbidden())
        self.assertVisit(WhenViewAddCtypeForm(user=self.user_alioth), ThenForbidden())
        self.assertVisit(WhenViewAddCtypeForm(user=None), ThenForbidden())
        for user in (self.user_admin, self.user_dd, self.user_alioth):
            self.assertVisit(WhenViewAddCtypeForm(user=user, member=True), ThenSuccess())

        # Empty posts (load form with validation messages)
        self.assertVisit(WhenPostIncompleteAddCtypeForm(user=self.user_admin), ThenSuccess())
        self.assertVisit(WhenPostIncompleteAddCtypeForm(user=self.user_dd), ThenCannotAdd())
        self.assertVisit(WhenPostIncompleteAddCtypeForm(user=self.user_alioth), ThenCannotAdd())
        self.assertVisit(WhenPostIncompleteAddCtypeForm(user=None), ThenCannotAdd())
        for user in (self.user_admin, self.user_dd, self.user_alioth):
            self.assertVisit(WhenPostIncompleteAddCtypeForm(user=user, member=True), ThenSuccess())

        # All members can add contribution types
        for user in (self.user_admin, self.user_dd, self.user_alioth):
            self.assertVisit(WhenSubmitAddCtypeForm(user=user, member=True), ThenCanAdd())

        # Among non-members, only superuser can add contribution type
        self.assertVisit(WhenSubmitAddCtypeForm(user=self.user_admin), ThenCanAdd())
        for user in (self.user_dd, self.user_alioth, None):
            self.assertVisit(WhenSubmitAddCtypeForm(user=user), ThenCannotAdd())

    def test_ctype_update(self):
        class WhenViewForm(DCTestUtilsWhen):
            url = reverse("source_ctype_update", kwargs={"sname": "test", "name": "tester"})
            ORIG_DESC = "tester_desc"
            ORIG_CDESC = "tester_cdesc"
            def _restore_source(self):
                ct = cmodels.ContributionType.objects.get(source__name="test", name="tester")
                ct.desc = self.ORIG_DESC
                ct.contrib_desc = self.ORIG_CDESC
                ct.save()
            def setUp(self, fixture):
                super(WhenViewForm, self).setUp(fixture)
                if self.user and self.args.get("member", False):
                    fixture.source.admins.add(self.user)
                self._restore_source()
            def tearDown(self, fixture):
                super(WhenViewForm, self).tearDown(fixture)
                fixture.source.admins.clear()
                self._restore_source()
        class WhenPostIncompleteForm(WhenViewForm):
            method = "post"
            def __unicode__(self):
                return "user {} posts an incomplete form to {}".format(self.user, self.url)
        class WhenPostForm(WhenViewForm):
            method = "post"
            data = {
                "name": "tester",
                "desc": "new_tester_desc",
                "contrib_desc": "new_tester_cdesc",
            }
            def __unicode__(self):
                return "user {} updates the contribution type at {}".format(self.user, self.url)
        class ThenCanUpdate(DCTestUtilsThen):
            def __call__(self, fixture, response, when, test_client):
                fixture.assertEquals(response.status_code, 302)
                fixture.assertEquals(response["Location"], "http://testserver/source/test/update/")
                try:
                    ct = fixture.source.contribution_types.get(name="tester")
                except cmodels.ContributionType.DoesNotExist:
                    ct = None
                fixture.assertIsNotNone(ct)
                fixture.assertEquals(ct.desc, when.data["desc"])
                fixture.assertEquals(ct.contrib_desc, when.data["contrib_desc"])
        class ThenCannotUpdate(ThenForbidden):
            def __call__(self, fixture, response, when, test_client):
                super(ThenCannotUpdate, self).__call__(fixture, response, when, test_client)
                try:
                    ct = fixture.source.contribution_types.get(name="tester")
                except cmodels.ContributionType.DoesNotExist:
                    ct = None
                fixture.assertIsNotNone(ct)
                fixture.assertEquals(ct.desc, "tester_desc")
                fixture.assertEquals(ct.contrib_desc, "tester_cdesc")

        # GETs
        self.assertVisit(WhenViewForm(user=self.user_admin), ThenSuccess())
        self.assertVisit(WhenViewForm(user=self.user_dd), ThenForbidden())
        self.assertVisit(WhenViewForm(user=self.user_alioth), ThenForbidden())
        self.assertVisit(WhenViewForm(user=None), ThenForbidden())
        for user in (self.user_admin, self.user_dd, self.user_alioth):
            self.assertVisit(WhenViewForm(user=user, member=True), ThenSuccess())

        # Empty posts (load form with validation messages)
        self.assertVisit(WhenPostIncompleteForm(user=self.user_admin), ThenSuccess())
        self.assertVisit(WhenPostIncompleteForm(user=self.user_dd), ThenCannotUpdate())
        self.assertVisit(WhenPostIncompleteForm(user=self.user_alioth), ThenCannotUpdate())
        self.assertVisit(WhenPostIncompleteForm(user=None), ThenCannotUpdate())
        for user in (self.user_admin, self.user_dd, self.user_alioth):
            self.assertVisit(WhenPostIncompleteForm(user=user, member=True), ThenSuccess())

        # All members can update a data source
        for user in (self.user_admin, self.user_dd, self.user_alioth):
            self.assertVisit(WhenPostForm(user=user, member=True), ThenCanUpdate())

        # Among non-members, only superuser can update a data source
        self.assertVisit(WhenPostForm(user=self.user_admin), ThenCanUpdate())
        for user in (self.user_dd, self.user_alioth, None):
            self.assertVisit(WhenPostForm(user=user), ThenCannotUpdate())

    def test_ctype_delete(self):
        class WhenCtypeDeleteBase(DCTestUtilsWhen):
            url = reverse("source_ctype_delete", kwargs={"sname": "test", "name": "umareller"})

            def setUp(self, fixture):
                super(WhenCtypeDeleteBase, self).setUp(fixture)
                cmodels.ContributionType.objects.filter(source=fixture.source, name="umareller").delete()
                self.ctype = cmodels.ContributionType.objects.create(source=fixture.source, name="umareller", desc="udesc", contrib_desc="ucdesc")
                fixture.source.admins.clear()
                if self.user and self.args.get("member", False):
                    fixture.source.admins.add(self.user)

            def tearDown(self, fixture):
                super(WhenCtypeDeleteBase, self).tearDown(fixture)
                cmodels.ContributionType.objects.filter(source=fixture.source, name="umareller").delete()
                fixture.source.admins.clear()

        class WhenViewCtypeDeleteForm(WhenCtypeDeleteBase):
            pass

        class WhenSubmitDeleteForm(WhenCtypeDeleteBase):
            method = "post"
            def __unicode__(self):
                return "user {} deletes a contribution type at {}".format(self.user, self.url)

        class ThenCanDelete(DCTestUtilsThen):
            def __call__(self, fixture, response, when, test_client):
                fixture.assertEquals(response.status_code, 302)
                fixture.assertEquals(response["Location"], "http://testserver/source/test/update/")
                try:
                    ct = fixture.source.contribution_types.get(name="umareller")
                except cmodels.ContributionType.DoesNotExist:
                    ct = None
                fixture.assertIsNone(ct)

        class ThenCannotDelete(ThenForbidden):
            def __call__(self, fixture, response, when, test_client):
                super(ThenCannotDelete, self).__call__(fixture, response, when, test_client)
                try:
                    ct = fixture.source.contribution_types.get(name="umareller")
                except cmodels.ContributionType.DoesNotExist:
                    ct = None
                fixture.assertIsNotNone(ct)

        # GETs
        self.assertVisit(WhenViewCtypeDeleteForm(user=self.user_admin), ThenSuccess())
        self.assertVisit(WhenViewCtypeDeleteForm(user=self.user_dd), ThenForbidden())
        self.assertVisit(WhenViewCtypeDeleteForm(user=self.user_alioth), ThenForbidden())
        self.assertVisit(WhenViewCtypeDeleteForm(user=None), ThenForbidden())
        for user in (self.user_admin, self.user_dd, self.user_alioth):
            self.assertVisit(WhenViewCtypeDeleteForm(user=user, member=True), ThenSuccess())

        # All members can update a data source
        for user in (self.user_admin, self.user_dd, self.user_alioth):
            self.assertVisit(WhenSubmitDeleteForm(user=user, member=True), ThenCanDelete())

        # Among non-members, only superuser can update a data source
        self.assertVisit(WhenSubmitDeleteForm(user=self.user_admin), ThenCanDelete())
        for user in (self.user_dd, self.user_alioth, None):
            self.assertVisit(WhenSubmitDeleteForm(user=user), ThenCannotDelete())

    def test_ctype_delete_contributions(self):
        class WhenDeleteContributionTypeContributions(WhenDeleteContributions):
            def setUp(self, fixture):
                super(WhenDeleteContributionTypeContributions, self).setUp(fixture)
                #### TODO: testing dates
                self.count_map = {
                    "source_contributions": 1,
                    "aggregated_source": 1,
                    "aggregated_person_contrib": 1,
                }
                self.url = reverse("source_ctype_delete_contributions",
                                   kwargs={ "sname": self.source.name, "name": self.ctype1.name })

        self.assertVisit(WhenDeleteContributionTypeContributions(user="admin"), ThenSuccessCountVerified())
        for u in "dd", "dd1", "alioth", "alioth1":
            self.assertVisit(WhenDeleteContributionTypeContributions(user=u), ThenForbidden())

        for u in "dd", "dd1", "alioth", "alioth1":
            self.assertVisit(WhenDeleteContributionTypeContributions(user=u, member_name=u), ThenSuccessCountVerified())

    def test_source_backup_list(self):
        class WhenView(DCTestUtilsWhen):
            url = reverse("source_backup_list", kwargs={"sname": "test"})
            def setUp(self, fixture):
                super(WhenView, self).setUp(fixture)
                if self.user and self.args.get("member", False):
                    fixture.source.admins.add(self.user)
            def tearDown(self, fixture):
                super(WhenView, self).tearDown(fixture)
                fixture.source.admins.clear()

        # GETs
        self.assertVisit(WhenView(user=self.user_admin), ThenSuccess())
        self.assertVisit(WhenView(user=self.user_dd), ThenForbidden())
        self.assertVisit(WhenView(user=self.user_alioth), ThenForbidden())
        self.assertVisit(WhenView(user=None), ThenForbidden())
        for user in (self.user_admin, self.user_dd, self.user_alioth):
            self.assertVisit(WhenView(user=user, member=True), ThenSuccess())

    def test_source_backup_download(self):
        class WhenView(DCTestUtilsWhen):
            url = reverse("source_backup_download", kwargs={"sname": "test", "backup_id": "20150208"})
            def setUp(self, fixture):
                super(WhenView, self).setUp(fixture)
                if self.user and self.args.get("member", False):
                    fixture.source.admins.add(self.user)
            def tearDown(self, fixture):
                super(WhenView, self).tearDown(fixture)
                fixture.source.admins.clear()

        # GETs
        self.assertVisit(WhenView(user=self.user_admin), ThenNotFound())
        self.assertVisit(WhenView(user=self.user_dd), ThenForbidden())
        self.assertVisit(WhenView(user=self.user_alioth), ThenForbidden())
        self.assertVisit(WhenView(user=None), ThenForbidden())
        for user in (self.user_admin, self.user_dd, self.user_alioth):
            self.assertVisit(WhenView(user=user, member=True), ThenNotFound())

# coding: utf8
# Debian Contributors source management functions
#
# Copyright (C) 2013--2014  Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# Create your views here.

from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals
from django import http, template, forms
from django.shortcuts import redirect, render, get_object_or_404
from django.utils.translation import ugettext as _
from django.views.generic import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView, ModelFormMixin
from django.core.urlresolvers import reverse, reverse_lazy
from django.core.exceptions import PermissionDenied
from django.http import StreamingHttpResponse
from django.core.servers.basehttp import FileWrapper
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.forms import ModelForm
import contributors.models as cmodels
import logging
import os

log = logging.getLogger(__name__)

def sources_list(request):
    sources = []
    have_user = request.user.is_authenticated()
    for s in cmodels.Source.objects.all().order_by("name"):
        if have_user:
            s.perm_can_admin = s.can_admin(request.user)
            s.perm_can_add_self = s.can_add_member(request.user, request.user)
        else:
            s.perm_can_admin = False
            s.perm_can_add_self = False
        sources.append(s)
    return render(request, "sources/list.html", {
        "sources": sources,
    })

def source_view(request, sname):
    from django.db.models import Min, Max

    source = get_object_or_404(cmodels.Source, name=sname)

    return render(request, "sources/view.html", {
        "source": source,
        "ctypes": source.contribution_types.all().order_by("desc"),
        "contribs": cmodels.AggregatedPersonContribution.objects.filter(ctype__source=source) \
                        .order_by("-until", "user__email"),
    })

def source_members(request, sname):
    source = get_object_or_404(cmodels.Source, name=sname)
    if not source.can_admin(request.user) and not source.can_add_member(request.user, request.user):
        raise http.Http404
    return render(request, "sources/members.html", {
        "source": source,
        "can_admin": source.can_admin(request.user),
    })

def source_members_add(request, sname):
    if request.method != "POST":
        return http.HttpResponseBadRequest(_("Only POST method is allowed"))
    source = get_object_or_404(cmodels.Source, name=sname)
    user = get_object_or_404(cmodels.User, email=request.POST.get("name", None))
    if not source.can_add_member(request.user, user):
        return http.HttpResponseForbidden(_("Can only add self"))
    source.admins.add(user)
    return redirect("source_members", sname=sname)

def source_members_delete(request, sname):
    if request.method != "POST":
        return http.HttpResponseBadRequest(_("Only POST method is allowed"))
    source = get_object_or_404(cmodels.Source, name=sname)
    user = get_object_or_404(cmodels.User, email=request.POST.get("name", None))
    if not source.can_admin(request.user):
        return http.HttpResponseForbidden(_("Cannot delete members"))
    source.admins.remove(user)
    return redirect("source_members", sname=sname)

def ctype_view(request, sname, name):
    source = get_object_or_404(cmodels.Source, name=sname)
    ct = get_object_or_404(cmodels.ContributionType, source=source, name=name)
    return render(request, "sources/cview.html", {
        "source": source,
        "ct": ct,
        "contribs": cmodels.AggregatedPersonContribution.objects.filter(ctype=ct) \
                        .order_by("-until", "user__email"),
    })

class SourceForm(ModelForm):
    class Meta:
        model = cmodels.Source
        exclude = ("last_import", "last_contribution", "admins",)

class SourceMixin(object):
    model = cmodels.Source
    slug_field = "name"
    slug_url_kwarg = "sname"
    form_class = SourceForm
    success_url = reverse_lazy('source_list')

    def get_object(self, queryset=None):
        source = super(SourceMixin, self).get_object(queryset)
        if source.can_admin(self.request.user):
            return source
        raise PermissionDenied

class SourceCreate(SourceMixin, CreateView):
    template_name = "sources/source_form.html"

    def dispatch(self, request, *args, **kw):
        if not request.user.is_authenticated(): raise PermissionDenied
        if not request.user.is_dd: raise PermissionDenied
        return super(SourceCreate, self).dispatch(request, *args, **kw)

    def form_valid(self, *args, **kw):
        res = super(SourceCreate, self).form_valid(*args, **kw)
        user = get_object_or_404(cmodels.User, pk=self.request.user.pk)
        self.object.admins.add(user)
        return res

class SourceUpdate(SourceMixin, UpdateView):
    template_name = "sources/source_form.html"

class SourceDelete(SourceMixin, DeleteView):
    template_name = "sources/source_confirm_delete.html"

class SourceBackupList(SourceMixin, DetailView):
    template_name = "sources/source_backup_list.html"

    def get_context_data(self, **kwargs):
        ctx = super(SourceBackupList, self).get_context_data(**kwargs)
        ctx["backups"] = list(self.object.backups())[::-1]
        return ctx

class SourceBackupDownload(SourceMixin, DetailView):
    def get(self, request, *args, **kwargs):
        source = self.get_object()
        backup_id = kwargs["backup_id"]
        backup_dir = source.get_backup_dir()
        if not backup_dir:
            raise http.Http404
        backup_file = os.path.join(backup_dir, backup_id + ".json.gz")
        if not os.path.exists(backup_file):
            raise http.Http404

        response = StreamingHttpResponse(FileWrapper(open(backup_file)),
                                content_type="application/json")
        response["Content-Length"] = os.path.getsize(backup_file)
        response["Content-Encoding"] = "gzip"
        response["Content-Disposition"] = "attachment; filename={}-{}.json".format(
            backup_id, source.name)
        return response

class ContributionTypeForm(ModelForm):
    class Meta:
        model = cmodels.ContributionType
        exclude = ('source',)

class ContributionTypeMixin(object):
    model = cmodels.ContributionType
    slug_field = "name"
    slug_url_kwarg = "name"
    form_class = ContributionTypeForm

    def get_queryset(self):
        qs = super(ContributionTypeMixin, self).get_queryset()
        qs = qs.filter(source__name=self.kwargs["sname"])
        return qs

    def get_success_url(self):
        obj = getattr(self, "object", None)
        if obj is not None:
            return reverse('source_update', kwargs={"sname": obj.source.name})
        else:
            return reverse('source_update', kwargs={"sname": self.kwargs["sname"]})

    def dispatch(self, request, *args, **kw):
        source = get_object_or_404(cmodels.Source, name=kw["sname"])
        if not source.can_admin(request.user):
            raise PermissionDenied
        return super(ContributionTypeMixin, self).dispatch(request, *args, **kw)

class ContributionTypeCreate(ContributionTypeMixin, CreateView):
    template_name = "sources/contributiontype_form.html"

    def form_valid(self, form):
        source = get_object_or_404(cmodels.Source, name=self.kwargs["sname"])
        form.instance.source = source
        return super(ContributionTypeCreate, self).form_valid(form)

class ContributionTypeUpdate(ContributionTypeMixin, UpdateView):
    template_name = "sources/contributiontype_form.html"

class ContributionTypeDelete(ContributionTypeMixin, DeleteView):
    template_name = "sources/contributiontype_confirm_delete.html"

class ContributionsDeleteView(ModelFormMixin, FormView):
    object = None

    ## We override directly post method instead form_valid,
    ## because lazy person does not want write an Empty form validable.
    ## In FormView/ProcessFormView is natural require form.is_valid to
    ## proceed with post, but we do not need to validate any data.
    def post(self, request, *args, **kwargs):
        ## check for permission "can_admin" is delegated to SourceMixin.get_object or
        ## ContributionTypeMixin.dispatch that would raise PermissionDenied.
        ## "dispatch" is already passed, if we are here, and we need to retrieve
        ## Source or ContributionType instance which also "get_object" will always
        ## run just now, so we do mot need to redo "can_admin" check.
        ##
        ## This implementation is "minimalist", not generic neither reusable.
        ## Assume to inherit from SourceMixin or ContributionTypeMixin,
        ## elsewise all is broken.

        self.object = self.get_object()  ## may me do check "can_admin" for SourceMixin

        if self.model is cmodels.Source:
            for ct in self.object.contribution_types.all():
                ct.contributions.all().delete()
                cmodels.AggregatedPersonContribution.recompute(ctype=ct)
            cmodels.AggregatedSource.recompute(source=self.object)
            cmodels.AggregatedPerson.recompute()
        elif self.model is cmodels.ContributionType:
            self.object.contributions.all().delete()
            cmodels.AggregatedPersonContribution.recompute(ctype=self.object)
            cmodels.AggregatedSource.recompute(source=self.object.source)
            cmodels.AggregatedPerson.recompute()
        else:
            ## this view is only for Source and ContributionType models
            ## this should not happen, is a ProgrammingError like server error
            raise http.Http500
        if callable(self.get_success_url):
            return http.HttpResponseRedirect(self.get_success_url())
        return http.HttpResponseRedirect(self.success_url)

    def get_context_data(self, **kwargs):
        ## this is only to provide a name in template
        if not self.object:
            self.object = self.get_object()
        return super(ContributionsDeleteView, self).get_context_data(**kwargs)


class SourceDeleteContributions(SourceMixin, ContributionsDeleteView):
    template_name = "sources/source_contributions_confirm_delete.html"

class ContributionTypeDeleteContributions(ContributionTypeMixin, ContributionsDeleteView):
    template_name = "sources/contributiontype_contributions_confirm_delete.html"

@login_required
def source_user_settings(request, sname):
    source = get_object_or_404(cmodels.Source, name=sname)

    if request.method == "POST":
        want_hidden = bool(request.POST.get("hide", False))
        settings = source.get_settings(request.user, create=want_hidden)
        if settings and settings.hidden != want_hidden:
            settings.hidden = want_hidden
            settings.save()
        for ctype in source.contribution_types.all():
            cmodels.AggregatedPersonContribution.recompute(user=request.user, ctype=ctype)
        cmodels.AggregatedSource.recompute(source=source)
        cmodels.AggregatedPerson.recompute(user=request.user)
        return redirect("source_view", sname=source.name)

    return render(request, "sources/source_user_settings.html", {
        "source": source,
        "settings": source.get_settings(request.user),
    })
